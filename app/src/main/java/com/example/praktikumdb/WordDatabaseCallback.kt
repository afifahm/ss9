package com.example.praktikumdb

private class WordDatabaseCallback(
    private val scope: CoroutineScope
) : RoomDatabase.Callback() {
    override fun onOpen(db: SupportSQLiteDatabase) {
        super.onOpen(db)
        INSTANCE?.let { database ->
            scope.launch {
                populateDatabase(database.wordDao())
            }
        }
    }
    suspend fun populateDatabase(wordDao: WordDao) {
// Delete all content here.
        wordDao.deleteAll()
// Add sample words.
        var word = Word("Hello")
        wordDao.insert(word)
        word = Word("World!")
        wordDao.insert(word)
// TODO: Add your own words!
    }
}