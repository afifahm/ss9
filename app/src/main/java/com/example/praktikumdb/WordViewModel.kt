package com.example.praktikumdb

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class WordViewModel (application: Application) : AndroidViewModel(application) {
    private val repository: WordRepository
    val allWords: LiveData<List<Word>>
    wordViewModel.allWords.observe(this, Observer { words ->
// Update the cached copy of the words in the adapter.
        words?.let { adapter.setWords(it) }
    })
    init {
        val wordDAO = WordRoomDatabase.getDatabase(application, viewModelScope).worddao()
        repository = WordRepository(wordDAO)
        allWords = repository.allWords
    }

    fun insert(word: Word) = viewModelScope.launch {
        repository.insert(word)
    }
}
