package com.example.praktikumdb

import androidx.room.ColumInfo
import androidx.room.Entity
import androidx.room.Primarykey

@Entity(tableName="word_table")
data class Word (@PrimaryKey @ColumnInfo (name = "word")val word: String)